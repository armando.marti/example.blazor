﻿namespace Application.VDC.UseCases.AvisoUC
{
    public class AvisoSuccessOutput : IUseCaseOutput
    {
        public AvisoSuccessOutput(int avisoId, bool success)
        {
            AvisoId = avisoId;
            Success = success;
        }

        public int AvisoId { get; }
        public bool Success { get; }
        
    }
    
}
