﻿namespace Application.VDC.UseCases
{
    public interface IOutputPortNotFound
    {
        void NotFound(string message);
    }
}
