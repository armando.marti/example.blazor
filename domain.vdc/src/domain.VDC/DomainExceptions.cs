﻿using System;

namespace Domain.VDC
{
    public class DomainException : Exception
    {
        public DomainException(string message) : base(message)
        {
        }
    }

    public class NotFoundException : DomainException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }

    [Serializable]
    public class DuplicateNameException : DomainException
    {

        public DuplicateNameException(string message) : base(message)
        {
        }
    }

    [Serializable]
    public class ReferenceException : DomainException
    {
        public ReferenceException(string message) : base(message)
        {
        }
    }

    [Serializable]
    public class WithReferencesException : DomainException
    {
        public WithReferencesException(string message) : base(message)
        {
        }
    }

    [Serializable]
    public class DuplicateReferenceException : DomainException
    {
        public DuplicateReferenceException(string message) : base(message)
        {
        }
    }
}
