﻿using System;
using System.Collections.Generic;

namespace Domain.VDC.Avisos
{
    public class AvisoService : IAvisoService
    {
        private readonly IAvisoFactory _factory;
        private readonly IAvisoPersistenceValidator _validator;

        public AvisoService(IAvisoFactory factory, IAvisoPersistenceValidator validator)
        {
            _factory = factory;
            _validator = validator;
        }

        public IAviso New(int? tipoAviso, int? categoriaAviso, string titulo, string texto, DateTime? fechaCreacion, DateTime? fechaDesde, DateTime? fechaHasta, bool? activo, List<AvisoUsuario> avisosUsuarios)
        {
            var entity = _factory.NewAviso(tipoAviso, categoriaAviso, titulo, texto, fechaCreacion, fechaDesde, fechaHasta, activo, avisosUsuarios);
            _validator.Validate(entity);
            return entity;
        }

        public IAviso New(IAviso item)
        {
            _validator.Validate(item);
            return item;
        }

        public IAviso Set(IAviso item, bool? activo=null, string leidoByUserName = null)
        {
            if (activo.HasValue) item.Activar(activo);
            if (string.IsNullOrWhiteSpace(leidoByUserName) == false) item.LeidoBy(leidoByUserName);
            _validator.Validate(item);
            return item;
        }
    }

}
