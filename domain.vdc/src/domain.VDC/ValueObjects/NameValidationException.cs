﻿using System;

namespace Domain.VDC.ValueObjects
{
    [Serializable]
    public sealed class NameValidationException : DomainException
    {
        public NameValidationException(string message) : base(message)
        {
        }
    }
}