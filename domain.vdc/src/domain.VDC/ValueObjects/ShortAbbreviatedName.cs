﻿namespace Domain.VDC.ValueObjects
{
    public readonly struct ShortAbbreviatedName
    {

        private readonly string _text;

        public ShortAbbreviatedName(string text)
        {
            if (string.IsNullOrWhiteSpace(text)) throw new NameValidationException("The 'Name' is required.");

            if (text.Length > 10) throw new NameValidationException("The 'Name' supports a maximum of 10 characters.");

            _text = text;
        }

        public override string ToString() => _text;


    }


}
