﻿namespace Domain.VDC.ValueObjects
{
    public readonly struct Identifier
    {

        public int Id { get; }

        public Identifier(int id)
        {
            if (id <= 0) throw new IdentifierValidationException("The identifier is required.");
            this.Id = id;
        }

        public override string ToString() => this.Id.ToString();
    }
}
