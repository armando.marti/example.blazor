namespace Api.VDC.DependencyInjection.FeatureFlags
{
    public enum Features
    {
        CreateTipoPoliza,
        UpdateTipoPoliza,
        GetTipoPoliza
    }
}