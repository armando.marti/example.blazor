﻿using Application.VDC.UseCases.AvisoUC;
using FluentMediator;
using Microsoft.Extensions.DependencyInjection;

namespace Api.VDC.DependencyInjection
{
    public static class FluentMediatorExtensions
    {
        public static IServiceCollection AddMediator(this IServiceCollection services)
        {
            services.AddFluentMediator(
                builder =>
                {
                    //Avisos
                    builder.On<UpdateLeidoAvisoInput>().PipelineAsync()
                        .Call<IUpdateLeidoAvisoUseCase>((handler, request) => handler.Execute(request));

                    builder.On<UpdateEstadoAvisoInput>().PipelineAsync()
                        .Call<IUpdateEstadoAvisoUseCase>((handler, request) => handler.Execute(request));

                    builder.On<CreateAvisoInput>().PipelineAsync()
                        .Call<ICreateAvisoUseCase>((handler, request) => handler.Execute(request));
                    
                });

            return services;
        }

    }
}
