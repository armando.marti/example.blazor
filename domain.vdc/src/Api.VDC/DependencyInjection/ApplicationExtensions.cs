﻿using Application.VDC.UseCases.AvisoUC;
using Application.VDC.UseCases.AvisoUC.Adaptadores;
using Microsoft.Extensions.DependencyInjection;

namespace Api.VDC.DependencyInjection
{
    public static class ApplicationExtensions
    {
        public static IServiceCollection AddUseCases(this IServiceCollection services)
        {           

            //avisos
            services.AddScoped<IUpdateLeidoAvisoUseCase, UpdateLeidoAvisoUseCase>();
            services.AddScoped<IUpdateEstadoAvisoUseCase, UpdateEstadoAvisoUseCase>();
            services.AddScoped<ICreateAvisoUseCase, CreateAvisoUseCase>();

            return services;

        }
    }
}
