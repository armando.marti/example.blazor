﻿using Application.VDC.UseCases.AvisoUC;
using FluentMediator;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Api.VDC.UseCases.Avisos.Update
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AvisosController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly AvisoPresenter _presenter;

        public AvisosController(IMediator mediator, AvisoPresenter presenter)
        {
            _mediator = mediator;
            _presenter = presenter;
        }

        /// <summary>
        /// Actualiza fecha leído el aviso para el usuario.
        /// </summary>
        /// <response code="200">La fecha leído del aviso se ha actualizado correctamente.</response>        
        /// <response code="400">Petición incorrecto.</response>
        /// <response code="404">Aviso no existe.</response>
        /// <response code="500">Error.</response>
        /// <param name="request">Petición para actualizar fecha leído de aviso de un usuario.</param>
        /// <returns>OkResult. </returns>
        [Route("[action]")]
        [HttpPatch("MarcarLeido")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> MarcarLeido(int avisoId, string user)
        {
            var input = new UpdateLeidoAvisoInput(avisoId, user);
            await _mediator.PublishAsync(input);
            return _presenter.ViewModel;

        }

        /// <summary>
        /// Actualizar estado del aviso.
        /// </summary>
        /// <response code="200">El estado del aviso se ha actualizado correctamente.</response>        
        /// <response code="400">Petición incorrecto.</response>
        /// <response code="404">Aviso no existe.</response>
        /// <response code="500">Error.</response>
        /// <param name="request">Petición para cambiar el estado del aviso.</param>
        /// <returns>OkResult. </returns>
        [Route("[action]")]
        [HttpPatch("CambiarEstado")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CambiarEstado(int avisoId, bool estado)
        {
            var input = new UpdateEstadoAvisoInput(avisoId, estado);
            await _mediator.PublishAsync(input);
            return _presenter.ViewModel;

        }
    }
}
