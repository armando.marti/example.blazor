﻿using Api.VDC.Models;
using Domain.VDC.Avisos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Api.VDC.UseCases.Avisos.Create
{
    public class CreateAvisoRequest
    {
        public CreateAvisoRequest(int id, string titulo,
            DateTime? fechaCreacion, DateTime? fechaDesde, DateTime? fechaHasta,
            int? avisoTipoId, int? avisoCategoriaId,
            string texto, bool? activo,
            List<AvisoUsuario> avisosUsuarios)
        {
            Id = id;
            Titulo = titulo;
            FechaCreacion = fechaCreacion;
            FechaDesde = fechaDesde;
            FechaHasta = fechaHasta;
            AvisoTipoId = avisoTipoId;
            AvisoCategoriaId = avisoCategoriaId;
            Texto = texto;
            Activo = activo;
            AvisosUsuarios = avisosUsuarios;
        }

        public int Id { get; set; }
        
        [Required]
        [MaxLength(50)]
        public string Titulo { get; set; }

        [Required]
        public DateTime? FechaCreacion { get; set; }
        
        [Required]
        public DateTime? FechaDesde { get; set; }
        
        [Required]
        public DateTime? FechaHasta { get; set; }
        
        [Required]
        public int? AvisoTipoId { get; set; }
        
        [Required]
        public int? AvisoCategoriaId { get; set; }
        
        [Required]
        [MaxLength(200)]
        public string Texto { get; set; }
        public bool? Activo { get; set; }

        [Required]        
        [HasElements]
        public List<AvisoUsuario> AvisosUsuarios { get; set; }
        
    }

    
}
