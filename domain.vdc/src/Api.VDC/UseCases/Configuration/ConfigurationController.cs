﻿using Infrastructure.VDC.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace Api.VDC.UseCases.Configuration
{
    [Route("api/[controller]")]
    [ApiController]
    public sealed class ConfigurationController : ControllerBase
    {
        private readonly ISeedDataService _service;
        private readonly IConfiguration _configuration;

        public ConfigurationController(ISeedDataService service, IConfiguration configuration)
        {
            _service = service;
            _configuration = configuration;
        }

        [HttpGet]
        public string Get()
        {
            var memory = _configuration.GetSection("DataMemory:active");
            if (memory.Value == "true")
            {
                return _service.Excute();
            }
            else
            {
                var msg = "No se aplica SeedData. La api no está en memoria";
                Log.Information(msg);
                return msg;

            }
        }
    }
}