using Api.VDC.DependencyInjection;
using Api.VDC.DependencyInjection.FeatureFlags;
using Infrastructure.VDC.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace Api.VDC
{
    public class Startup
    {
        private readonly IWebHostEnvironment _environment;
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            _environment = environment;            
        }

       

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            var memory = Configuration.GetSection("DataMemory:active");

            services.AddControllers().AddControllersAsServices();
            services.AddMvc().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
            });

            services.AddBusinessExceptionFilter();
            services.AddFeatureFlags(Configuration);

            services.AddDomainServices();

            services.AddSwaggerGen();
            //services.AddVersioning();

            services.AddUseCases();

            if (memory.Value == "true")
            {
                services.AddSQLServerInMemory(Configuration);                
            }
            else
            {
                services.AddSQLServerPersistence(Configuration);                
            }

            services.AddPresenters();
            services.AddMediator();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllHeaders",
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:5001")
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                    });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, CedContext adminContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            if (adminContext.Database.IsInMemory())
            {
                Log.Information("Api enlazada a memoria.");
                
                adminContext.Database.EnsureCreated();

                Log.Information("Database creada en memoria.");
            }
            else
            {
                Log.Information("Api enlazada a base de datos.");
            }

            app.UseSerilogRequestLogging();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors("AllowAllHeaders");

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "VDC API V1");
                //c.RoutePrefix = string.Empty; // => http://localhost:<port>/
            });


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
