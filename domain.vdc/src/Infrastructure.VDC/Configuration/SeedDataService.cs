﻿using Infrastructure.VDC.Data;

namespace Infrastructure.VDC.Configuration
{
    public interface ISeedDataService
    {
        string Excute();
    }

    public class SeedDataService : ISeedDataService
    {
        private readonly CedContext _context;

        public SeedDataService(CedContext context)
        {
            _context = context;
            context.Database.EnsureCreated();
        }

        public string Excute()
        {
            return "SeedData execute";
        }
    }
}
