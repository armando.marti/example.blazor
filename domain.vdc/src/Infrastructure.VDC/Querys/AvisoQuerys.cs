﻿using Domain.VDC.Avisos;
using Infrastructure.VDC.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Infrastructure.VDC.Querys
{
    public class AvisoQuerys : IAvisoQuerys
    {
        private CedContext _context;

        public AvisoQuerys(CedContext context)
        {
            _context = context;
        }


        public IQueryable<AvisoDTO> GetCabecerasAvisosFiltered(string search = "", StringComparison stringComparer = StringComparison.OrdinalIgnoreCase)
        {

            IQueryable<AvisoDTO> data = null;

            var sqlDB = _context.Database.IsSqlServer();

            if (sqlDB is false)
            {
                data = from a in _context.Avisos
                       join c in _context.AvisosCategoria on a.AvisoCategoriaId equals c.Id into AvisoCatgoria
                       from ac in AvisoCatgoria.DefaultIfEmpty()
                       join t in _context.AvisosTipo on a.AvisoTipoId equals t.Id into AvisoTipo
                       from at in AvisoTipo.DefaultIfEmpty()
                       where a.Id.ToString().Contains(search,stringComparer)
                        || a.Titulo.Contains(search, stringComparer)
                        || ac.Descripcion.Contains(search, stringComparer)
                        || at.Descripcion.Contains(search, stringComparer)
                       select new AvisoDTO
                       {
                           Id = a.Id,
                           Titulo = a.Titulo,
                           Texto = a.Texto,
                           Activo = a.Activo,
                           FechaCreacion = a.FechaCreacion,
                           FechaDesde = a.FechaDesde,
                           FechaHasta = a.FechaHasta,
                           CategoriaId = a.AvisoCategoriaId,
                           Categoria = ac.Descripcion,
                           TipoId = a.AvisoTipoId,
                           Tipo = at.Descripcion,
                           StringId = a.Id.ToString()
                       };
            }
            else
            {
                string sql = "Select * from ListAvisos";
                data = _context.QAvisos.FromSqlRaw(sql)
                    .Where(w => w.StringId.Contains(search) || w.Titulo.Contains(search) || w.Categoria.Contains(search) || w.Tipo.Contains(search));
            }

            return data;
        }

        public IQueryable<Aviso> GetAvisos()
        {
            return _context.Avisos.Include(i => i.AvisosUsuarios);              
            
        }

        public IQueryable<AvisoDTO> GetCabecerasAvisos(bool? activo = null, bool? leidos = null, int? tipo = null, string usuario = "", DateTime? fDesde = null, DateTime? fHasta = null)
        {
            
            
            return from a in _context.Avisos
                   join c in _context.AvisosCategoria on a.AvisoCategoriaId equals c.Id into AvisoCatgoria
                   from ac in AvisoCatgoria.DefaultIfEmpty()

                   join t in _context.AvisosTipo on a.AvisoTipoId equals t.Id into AvisoTipo                   
                   from at in AvisoTipo.DefaultIfEmpty()                   

                   join u in _context.AvisosUsuario on a.Id equals u.AvisoId into AvisoUser
                   from au in AvisoUser.DefaultIfEmpty()
                   
                   where (
                        (a.Activo == activo || activo == null)
                        && ((au.UserName == usuario || string.IsNullOrWhiteSpace(usuario)) && (au.FechaLeido.HasValue == leidos || leidos == null) ) 
                        && (a.AvisoTipoId == tipo || tipo == null)
                        && ((a.FechaDesde <= fDesde || fDesde == null) && (a.FechaHasta >= fHasta || fHasta == null))  
                   )
                   
                   select new AvisoDTO
                   {
                       Id = a.Id,
                       Titulo = a.Titulo,
                       Texto = a.Texto,
                       Activo = a.Activo,
                       FechaCreacion = a.FechaCreacion,
                       FechaDesde = a.FechaDesde,
                       FechaHasta = a.FechaHasta,
                       CategoriaId = a.AvisoCategoriaId,
                       Categoria = ac.Descripcion,
                       TipoId = a.AvisoTipoId,                       
                       Tipo = at.Descripcion,                       
                       UserName = au.UserName,
                       FechaLeido = au.FechaLeido
                   };
        }

        public IQueryable<AvisoCategoria> GetCategoriasAvisos()
        {
            return _context.AvisosCategoria;
        }

        public IQueryable<AvisoTipo> GetTiposAvisos()
        {
            return _context.AvisosTipo;
        }
    }
}
