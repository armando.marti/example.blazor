﻿using Domain.VDC;
using Domain.VDC.Avisos;
using Infrastructure.VDC.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.VDC.Repositories
{
    public class AvisoRepository : BaseRepository<Aviso, IAviso>, IAvisoRepository
    {
        protected readonly int _tipo;

        public AvisoRepository(CedContext context) : base(context, "aviso")
        {
        }

        public async Task<IAviso> GetWithUsersBy(int id)
        {
            if (id == -1) return GetDefault();

            IAviso result = await _context.Avisos.Where(x => x.Id == id).Include(i=>i.AvisosUsuarios).SingleOrDefaultAsync();

            if (result is null)
            {
                throw new NotFoundException($"The {_itemName} {id} does not exist or is not processed yet.");
            }

            return result;
        }

    }
}