﻿using Domain.VDC;
using Domain.VDC.Avisos;
using Infrastructure.VDC.Data;
using Infrastructure.VDC.Validators;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using NUnit.Framework;
using System;

namespace Test.Domain.VCD.Infraestructure.PersistenceValidators
{
    [SetCulture("es-ES")]
    public class AvisoValidatorTest
    {   
        private IAvisoPersistenceValidator _validator;
        

        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<CedContext>()
                .UseInMemoryDatabase(databaseName: "memory")
                .ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;

            var context = new CedContext(options);
            context.Database.EnsureCreated();

            
            _validator = new AvisoPersistenceValidator(context);
        }
        [Test]
        public void Test_notFound()
        {
            var entity = new Aviso(99 ,1, 1, null, null, null, "", "", true);
            var ex = Assert.Throws<NotFoundException>(() => _validator.Validate(entity));
            Assert.AreEqual($"The aviso {entity.Id} does not exist or is not processed yet.", ex.Message);
        }

        [Test]
        public void Test_required_data()
        {
            var entity = new Aviso(1, null, null, null, null, null, "", "", null);
            var ex = Assert.Throws<DomainException>(() => _validator.Validate(entity));
            Assert.AreEqual($"Título debe tener valor.", ex.Message);

            entity = new Aviso(1, null, null, null, null, null, "titulo", "", null);
            ex = Assert.Throws<DomainException>(() => _validator.Validate(entity));
            Assert.AreEqual($"Texto debe tener valor.", ex.Message);

            entity = new Aviso(1, null, null, null, null, null, "titulo", "texto", null);
            ex = Assert.Throws<DomainException>(() => _validator.Validate(entity));
            Assert.AreEqual($"Tipo debe tener valor.", ex.Message);

            entity = new Aviso(1, 1, null, null, null, null, "titulo", "texto", null);
            ex = Assert.Throws<DomainException>(() => _validator.Validate(entity));
            Assert.AreEqual($"Categoria debe tener valor.", ex.Message);

            entity = new Aviso(1, 1, 1, null, null, null, "titulo", "texto", null);
            ex = Assert.Throws<DomainException>(() => _validator.Validate(entity));
            Assert.AreEqual($"Fecha Creación debe tener valor.", ex.Message);

            entity = new Aviso(1, 1, 1, DateTime.Now, null, null, "titulo", "texto", null);
            ex = Assert.Throws<DomainException>(() => _validator.Validate(entity));
            Assert.AreEqual($"Fecha Desde debe tener valor.", ex.Message);

            entity = new Aviso(1, 1, 1, DateTime.Now, DateTime.Now, null, "titulo", "texto", null);
            ex = Assert.Throws<DomainException>(() => _validator.Validate(entity));
            Assert.AreEqual($"Fecha Hasta debe tener valor.", ex.Message);

            entity = new Aviso(1, 1, 1, DateTime.Now, DateTime.Now, DateTime.Now.AddMinutes(1), "titulo", "texto", null);
            ex = Assert.Throws<DomainException>(() => _validator.Validate(entity));
            Assert.AreEqual($"Debe indicarse al menos un destinatario.", ex.Message);

            
        }

        [Test]
        public void Test_references()
        {
            var entity = new Aviso(1, 99, 99, DateTime.Now, DateTime.Now, DateTime.Now.AddMinutes(1), "titulo", "texto", null);
            entity.AddUser("usuario01");
            var ex = Assert.Throws<ReferenceException>(() => _validator.Validate(entity));
            Assert.AreEqual($"'AvisoTipoId: 99'", ex.Message);

            entity = new Aviso(1, 1, 99, DateTime.Now, DateTime.Now, DateTime.Now.AddMinutes(1), "titulo", "texto", null);
            entity.AddUser("usuario01");
            ex = Assert.Throws<ReferenceException>(() => _validator.Validate(entity));
            Assert.AreEqual($"'AvisoCategoriaId: 99'", ex.Message);
        }

        [Test]
        public void Test_ranges()
        {
            var entity = new Aviso(1, 1, 1, DateTime.Now, DateTime.Now, DateTime.Now.AddMinutes(-1), "titulo", "texto", null);
            var ex = Assert.Throws<DomainException>(() => _validator.Validate(entity));
            Assert.AreEqual($"Fecha Hasta no puede ser menor a la actual.", ex.Message);

            entity = new Aviso(1, 1, 1, DateTime.Now, DateTime.Now.AddMinutes(2), DateTime.Now.AddMinutes(1), "titulo", "texto", null);
            ex = Assert.Throws<DomainException>(() => _validator.Validate(entity));
            Assert.AreEqual($"Fecha Hasta no puede ser inferior a Fecha Desde.", ex.Message);
        }




    }
}
