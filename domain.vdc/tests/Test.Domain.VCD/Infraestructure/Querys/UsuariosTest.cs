﻿using Domain.VDC.Usuarios;
using Infrastructure.VDC.Data;
using Infrastructure.VDC.Querys;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System.Linq;

namespace Test.Domain.VCD.Infraestructure.Querys
{
    public class UsuariosTest
    {
        private IUsuarioQuerys _querys;
        private static readonly ILoggerFactory logger = LoggerFactory.Create(builder => { builder.AddConsole(); });

        [SetUp]
        public void Setup()
        {

            
            
            #region Memory
            var options = new DbContextOptionsBuilder<CedContext>()
              .UseInMemoryDatabase(databaseName: "memory")
              .ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning))
              .Options;

            var context = new CedContext(options);
            context.Database.EnsureCreated();
            #endregion

            #region DataBase
            //var configuration = TestHelper.GetIConfigurationRoot(TestContext.CurrentContext.TestDirectory);            
            //var password = configuration["DbPassword"];
            //var options = new DbContextOptionsBuilder<CedContext>()
            //   .UseLoggerFactory(logger)
            //   .UseSqlServer(configuration.GetConnectionString("DefaultConnection").Replace("<DbPassword>", configuration["DbPassword"]))
            //   .Options;

            //var context = new CedContext(options);
            #endregion


            _querys = new UsuarioQuerys(context);
        }

        [Test]
        public void Test_list_usuarios()
        {
            var result = _querys.GetUsuarios().ToList();

            Assert.AreEqual(5, result.Count());
        }

        [Test]
        public void Test_list_roles()
        {
            var result = _querys.GetRoles().ToList();
            Assert.AreEqual(3, result.Count());
        }

        [Test]
        public void Test_list_usuarios_externos()
        {
            var result = _querys.GetUsuariosExternos().ToList();

            Assert.AreEqual(2, result.Count());
        }
    }
}
