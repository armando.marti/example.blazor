﻿using Microsoft.Extensions.Configuration;

namespace Test.Domain.VCD
{
    public static class TestHelper
    {
        public static IConfigurationRoot GetIConfigurationRoot(string outputPath)
        {
            return new ConfigurationBuilder()
                .SetBasePath(outputPath)
                .AddJsonFile("appsettings.Development.json", optional: true)
                .AddUserSecrets("33ae21d6-2232-455a-a735-f4795f533df1")                
                .AddEnvironmentVariables()
                .Build();
        }
    }
}
