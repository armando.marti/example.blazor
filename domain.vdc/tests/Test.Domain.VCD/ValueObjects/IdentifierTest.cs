﻿using Domain.VDC.ValueObjects;
using NUnit.Framework;

namespace Test.Domain.VCD.ValueObjects
{
    public class IdentifierTest
    {
        [Test]
        public void Identifier_Set_Test()
        {
            var id = new Identifier(1);
            Assert.AreEqual("1", id.ToString());

            
            var ex = Assert.Throws<IdentifierValidationException>(() => new Identifier(0));
            Assert.AreEqual("The identifier is required.", ex.Message);
        }
    }
}
