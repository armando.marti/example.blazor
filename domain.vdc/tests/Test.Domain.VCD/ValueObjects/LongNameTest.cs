﻿using Domain.VDC.ValueObjects;
using NUnit.Framework;
using System;

namespace Test.Domain.VCD.ValueObjects
{
    public class LongNameTest
    {
        [SetUp]
        public void Setup()
        {
        }


        [Test]
        public void Test_name_validation()
        {
            var ex = Assert.Throws<NameValidationException>(() => new LongName(""));
            Assert.That(ex.Message == "The 'Name' is required.", ex.Message);

            ex = Assert.Throws<NameValidationException>(() => new LongName(new String('X', 101)));
            Assert.That(ex.Message == "The 'Name' supports a maximum of 100 characters.", ex.Message);
        }

        [Test]
        public void Test_name_comparer()
        {
            //correct names comparer
            var name1 = new LongName("Manuel");
            var name2 = new LongName("Manuel");

            Assert.IsTrue(name1.ToString() == "Manuel");
            Assert.IsTrue(name2.ToString() == "Manuel");

            Assert.AreEqual(name1, name2);
        }

    }
}
