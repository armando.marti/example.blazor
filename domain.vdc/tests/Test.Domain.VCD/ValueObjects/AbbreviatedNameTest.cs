﻿using Domain.VDC.ValueObjects;
using NUnit.Framework;
using System;

namespace Test.Domain.VCD.ValueObjects
{
    public class AbbreviatedNameTest
    {
        [SetUp]
        public void Setup()
        {
        }


        [Test]
        public void Test_ShortAbbreviatedName_validation()
        {
            var ex = Assert.Throws<NameValidationException>(() => new ShortAbbreviatedName(""));
            Assert.That(ex.Message == "The 'Name' is required.", ex.Message);

            ex = Assert.Throws<NameValidationException>(() => new ShortAbbreviatedName(new String('X', 11)));
            Assert.That(ex.Message == "The 'Name' supports a maximum of 10 characters.", ex.Message);
        }

        [Test]
        public void Test_ShortAbbreviatedName_comparer()
        {
            //correct names comparer
            var name1 = new ShortAbbreviatedName("Manuel");
            var name2 = new ShortAbbreviatedName("Manuel");

            Assert.IsTrue(name1.ToString() == "Manuel");
            Assert.IsTrue(name2.ToString() == "Manuel");

            Assert.AreEqual(name1, name2);
        }

        [Test]
        public void Test_abbreviatedName_validation()
        {
            var ex = Assert.Throws<NameValidationException>(() => new AbbreviatedName(""));
            Assert.That(ex.Message == "The 'Name' is required.", ex.Message);

            ex = Assert.Throws<NameValidationException>(() => new AbbreviatedName(new String('X', 16)));
            Assert.That(ex.Message == "The 'Name' supports a maximum of 15 characters.", ex.Message);
        }

        [Test]
        public void Test_abbreviatedName_comparer()
        {
            //correct names comparer
            var name1 = new AbbreviatedName("Manuel");
            var name2 = new AbbreviatedName("Manuel");

            Assert.IsTrue(name1.ToString() == "Manuel");
            Assert.IsTrue(name2.ToString() == "Manuel");

            Assert.AreEqual(name1, name2);
        }

    }
}
