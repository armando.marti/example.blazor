﻿using Domain.VDC.ValueObjects;
using NUnit.Framework;
using System;

namespace Test.Domain.VCD.ValueObjects
{
    public class ShortNameTest
    {
        [SetUp]
        public void Setup()
        {
        }


        [Test]
        public void Test_shortname_validation()
        {
            var ex = Assert.Throws<NameValidationException>(() => new ShortName(""));
            Assert.That(ex.Message == "The 'Name' is required.", ex.Message);

            ex = Assert.Throws<NameValidationException>(() => new ShortName(new String('X', 26)));
            Assert.That(ex.Message == "The 'Name' supports a maximum of 25 characters.", ex.Message);
        }

        [Test]
        public void Test_shortname_comparer()
        {
            //correct names comparer
            var name1 = new ShortName("Manuel");
            var name2 = new ShortName("Manuel");

            Assert.IsTrue(name1.ToString() == "Manuel");
            Assert.IsTrue(name2.ToString() == "Manuel");

            Assert.AreEqual(name1, name2);

        }

    }
}
