﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Web.Vdc.Doctor.DataMemory;
using Web.Vdc.Doctor.Services;
using Web.Vdc.Doctor.ViewModels;

namespace web.vdc.test.ViewModels
{
    public class AvisoViewModelTests
    {

        private IAvisoCambioEstadoService _avisoCambioEstadoService;
        private IAvisoCategoriaService _avisoCategoriaService;
        private IAvisosMarcarLeidoService _avisosMarcarLeidoService;
        private IAvisoTipoService _avisoTipoService;
        private IUsuarioService _usuarioService;
        private IAvisoService _avisoService;
        private IAvisoViewModel _avisoViewModel;

        [SetUp]
        public void Setup()
        {
            _avisoCambioEstadoService = new AvisoCambioEstadoService();
            _avisoCategoriaService = new AvisoCategoriaService();
            _avisosMarcarLeidoService = new AvisoMarcarLeidoService();
            _avisoTipoService = new AvisoTipoService();
            _usuarioService = new UsuarioService();
            _avisoService = new AvisoService(_avisoTipoService, _avisoCategoriaService, _usuarioService, _avisoCambioEstadoService);
            _avisoViewModel = new AvisoViewModel(_avisoService);
        }

        [Test]
        public void LoadModal_debe_obtener_modelos()
        {
                        
            _avisoViewModel.LoadModel().GetAwaiter().GetResult();  
            

            //nodos roles
            Assert.AreEqual(3, _avisoViewModel.UsuariosList.TreeItems.Count);
            
            //categorias
            Assert.AreEqual(3, _avisoViewModel.CategoriasList.Count);
            
            //tipos
            Assert.AreEqual(4, _avisoViewModel.TiposList.Count);
        }

        [Test]
        public void SetItem_1_debe_establecer_id_1_estado_browsing()
        {
            _avisoViewModel.LoadModel().GetAwaiter().GetResult();

            _avisoViewModel.SetItem(1);

            Assert.AreEqual(1, _avisoViewModel.AvisoItem.Id);
            Assert.AreEqual(1, _avisoViewModel.AvisoClone.Id);
            Assert.AreEqual(ModelState.Browsing, _avisoViewModel.State);
            Assert.AreEqual(false, _avisoViewModel.IsBusy);
        }

        [Test]
        public void SetItem_0_debe_establecer_id_0_estado_creating()
        {
            _avisoViewModel.LoadModel().GetAwaiter().GetResult();

            _avisoViewModel.SetItem(0);

            Assert.AreEqual(0, _avisoViewModel.AvisoItem.Id);
            Assert.AreEqual(0, _avisoViewModel.AvisoClone.Id);
            Assert.AreEqual(ModelState.Creating, _avisoViewModel.State);
            Assert.AreEqual(false, _avisoViewModel.IsBusy);
        }

        [Test]
        public void ChangeStateItem_establece_activo()
        {

            var avisoId = 2;
            
            _avisoViewModel.LoadModel().GetAwaiter().GetResult();
            _avisoViewModel.ChangeStateItem(avisoId, false).GetAwaiter().GetResult();

            Assert.AreEqual(false, _avisoViewModel.AvisosList.FirstOrDefault(av => av.Id == avisoId)?.Activo);
            Assert.AreEqual(ModelState.Browsing, _avisoViewModel.State);
            Assert.AreEqual(false, _avisoViewModel.IsBusy);
        }

        [Test]
        public void Cancel_debe_cancelar_cambios()
        {
            _avisoViewModel.LoadModel().GetAwaiter().GetResult();
            _avisoViewModel.SetItem(1).GetAwaiter().GetResult();
            
            _avisoViewModel.AvisoItem.AvisoTipoId = 3;

            _avisoViewModel.CancelChanges().GetAwaiter().GetResult();

            Assert.AreNotEqual(3, _avisoViewModel.AvisoItem.AvisoTipoId);
            Assert.IsTrue(_avisoViewModel.AvisoItem.PropertiesAreEquals(_avisoViewModel.AvisoClone));
            Assert.AreEqual(ModelState.Browsing, _avisoViewModel.State);
            Assert.AreEqual(false, _avisoViewModel.IsBusy);
        }

        [Test]
        public void Save_debe_guardar_cambios()
        {
            _avisoViewModel.LoadModel().GetAwaiter().GetResult();
            _avisoViewModel.SetItem(1).GetAwaiter().GetResult();
                        
            _avisoViewModel.AvisoItem.AvisoTipoId = 3;

            _avisoViewModel.SaveChanges().GetAwaiter().GetResult();


            Assert.AreEqual(3, _avisoViewModel.AvisoItem.AvisoTipoId);
            Assert.AreEqual(ModelState.Browsing, _avisoViewModel.State);
            Assert.AreEqual(false, _avisoViewModel.IsBusy);
        }

        [Test]
        public void Save_nuevoAviso_debe_crearNuevoAviso()
        {
            _avisoViewModel.LoadModel().GetAwaiter().GetResult();
            _avisoViewModel.SetItem(0).GetAwaiter().GetResult();

            _avisoViewModel.AvisoItem.AvisoTipoId = 1;
            

            _avisoViewModel.SaveChanges().GetAwaiter().GetResult();


            Assert.AreEqual(_avisoViewModel.AvisoItem.FechaCreacion.Value.AddMinutes(-5), _avisoViewModel.AvisoItem.FechaDesde);
            Assert.AreEqual(_avisoViewModel.AvisoItem.FechaCreacion.Value.AddMinutes(5), _avisoViewModel.AvisoItem.FechaHasta);
            Assert.AreEqual(true, _avisoViewModel.AvisoItem.Activo);            
            Assert.GreaterOrEqual(_avisoViewModel.AvisoItem.Id, 0);
            Assert.IsTrue(_avisoViewModel.AvisosList.Any(av => av.Id == _avisoViewModel.AvisoItem.Id));

            
            Assert.AreEqual(ModelState.Browsing, _avisoViewModel.State);
            Assert.AreEqual(false, _avisoViewModel.IsBusy);
        }


        [Test]
        public void Duplicate_aviso_debe_duplicar_aviso()
        {
            _avisoViewModel.SetItem(1).GetAwaiter().GetResult();
            var origen = _avisoViewModel.AvisoItem;
            
            _avisoViewModel.DuplicateItem(1).GetAwaiter().GetResult();

            Assert.GreaterOrEqual(DateTime.Now, _avisoViewModel.AvisoItem.FechaCreacion);
            Assert.AreEqual(0, _avisoViewModel.AvisoItem.Id);
            CollectionAssert.AreEqual(origen.AvisosUsuarios, _avisoViewModel.AvisoItem.AvisosUsuarios);
            Assert.AreEqual(ModelState.Creating, _avisoViewModel.State);
            Assert.AreEqual(false, _avisoViewModel.IsBusy);

        }


    }
}
