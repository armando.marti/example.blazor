﻿using System.Text.Json;

namespace web.vdc.test
{
    public static class ObjectExtensions
    {
        public static bool PropertiesAreEquals(this object actual, object other)
        {
            var auxActual = JsonSerializer.Serialize(actual);
            var auxOther = JsonSerializer.Serialize(other);

            return auxActual == auxOther;
        }
    }
}
