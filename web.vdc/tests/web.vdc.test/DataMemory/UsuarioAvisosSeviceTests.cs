﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Web.Vdc.Doctor.DataMemory;
using Web.Vdc.Doctor.Models.GetAvisos;
using Web.Vdc.Doctor.Services;

namespace web.vdc.test.DataMemory
{
    public class UsuarioAvisosSeviceTests
    {
        private IUsuariosAvisosService _service;

        [SetUp]
        public void Setup()
        {
            _service = new UsuariosAvisosService();

        }

        [Test]
        public void Test_getAviso_return_list_avisos()
        {
            var request = new RequestUsuariosAvisos("user",false,DateTime.Now);
            _service.Request = request;

            var result = _service.GetAviso().GetAwaiter().GetResult();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Texto Aviso", result[0].Texto);
            Assert.AreEqual("Titulo aviso", result[0].Titulo);
        }
    }
}
