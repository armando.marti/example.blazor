﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Web.Vdc.Doctor.DataMemory;
using Web.Vdc.Doctor.Models.Avisos;
using Web.Vdc.Doctor.Services;
using Web.Vdc.Doctor.Exceptions;

namespace web.vdc.test.DataMemory
{
    public class AvisoServiceTests
    {
        private IAvisoCambioEstadoService _avisoCambioEstadoService;
        private IAvisoCategoriaService _avisoCategoriaService;
        private IAvisosMarcarLeidoService _avisosMarcarLeidoService;
        private IAvisoTipoService _avisoTipoService;
        private IUsuarioService _usuarioService;
        private IAvisoService _avisoService;

        [SetUp]
        public void Setup()
        {
            _avisoCambioEstadoService = new AvisoCambioEstadoService();
            _avisoCategoriaService = new AvisoCategoriaService();
            _avisosMarcarLeidoService = new AvisoMarcarLeidoService();
            _avisoTipoService = new AvisoTipoService();
            _usuarioService = new UsuarioService();
            _avisoService = new AvisoService(_avisoTipoService, _avisoCategoriaService, _usuarioService, _avisoCambioEstadoService);
        }
        
        [Test]
        public void SourceUsuarios_debe_obtener_usuarios()
        {
            var users = _avisoService.SourceUsuarios.GetAwaiter().GetResult();

            Assert.AreEqual(6, users.Count);
        }
        
        [Test]
        public void Aviso_sourceCategorias_debe_obtener_categorias()
        {

            var categorias = _avisoService.SourceCategorias.GetAwaiter().GetResult();

            Assert.AreEqual(3, categorias.Count);

        }

        [Test]
        public void SourceTipos_debe_obtener_tipos()
        {
            var tipos = _avisoTipoService.GetTiposAvisosAsync().GetAwaiter().GetResult();

            Assert.AreEqual(4, tipos.Count);
        }

        [Test]
        public void GetAviso_debe_obtener_aviso_1()
        {

            var aviso = new Aviso
            {
                Id = 1,
                Titulo = "Aviso inmediato 01",
                AvisoCategoriaId = 1,
                AvisoTipoId = 1,
                Activo = true,
                FechaCreacion = new DateTime(2020, 1, 1),
                FechaDesde = new DateTime(2020,1,1).AddSeconds(5),
                FechaHasta = new DateTime(2020, 1, 1).AddDays(10),
                Texto = "Aviso inmediato 01 informativo",
                AvisosUsuarios = new List<AvisoUsuario>()
               {
                 new AvisoUsuario {Id = 1, AvisoId = 1, UserName = "User01", FechaLeido = null},
                 new AvisoUsuario {Id = 2, AvisoId = 1, UserName = "User02", FechaLeido = null}
               }
            };

            var result = _avisoService.GetAsync(1).GetAwaiter().GetResult();

            Assert.IsTrue(aviso.PropertiesAreEquals(result));

        }

        [Test]
        public void GetAviso_100_debe_dar_exception()
        {   
            var ex =  Assert.Throws<ApiException>(() => _avisoService.GetAsync(100).GetAwaiter().GetResult());

            Assert.AreEqual("Aviso no existe.", ex.Message);
        }

        [Test]
        public void UpdateAviso_debe_actualizar_aviso_2()
        {
            var avisoUpdate = new Aviso
            {
                Id = 2,
                Titulo = "Aviso inmediato 02 - update",
                AvisoCategoriaId = 2,
                AvisoTipoId = 2,
                Activo = false,
                FechaCreacion = DateTime.MinValue.AddDays(1),
                FechaDesde = DateTime.MinValue.AddDays(10),
                FechaHasta = DateTime.MinValue.AddDays(15),
                Texto = "Aviso inmediato 01 informativo - update",
                AvisosUsuarios = new List<AvisoUsuario>()
               {
                 new AvisoUsuario {Id = 3, AvisoId = 2, UserName = "User05", FechaLeido = DateTime.MinValue.AddHours(1)},
                 new AvisoUsuario {Id = 4, AvisoId = 2, UserName = "User06", FechaLeido = null}
               }
            };

            _avisoService.UpdateAsync(avisoUpdate).GetAwaiter().GetResult();
            var result = _avisoService.GetAsync(2).GetAwaiter().GetResult();

            Assert.IsTrue(avisoUpdate.PropertiesAreEquals(result));
        }

        [Test]
        public void UpdateAviso_100_debe_dar_exception()
        {
            var aviso = new Aviso { Id = 100 };
            var ex = Assert.Throws<ApiException>(() => _avisoService.UpdateAsync(aviso).GetAwaiter().GetResult());

            Assert.AreEqual("Aviso no existe.", ex.Message);
        }

        [Test]
        public void CambiarEstadoAviso_aviso_2_debeActivarse()
        {
            _avisoService.CambiarEstado(1, true).GetAwaiter().GetResult();

            var result = _avisoService.GetAsync(1).GetAwaiter().GetResult();

            Assert.AreEqual(true, result.Activo);

        }

        [Test]
        public void CambiarEstadotAviso_100_debe_dar_exception()
        {
            var ex = Assert.Throws<ApiException>(() => _avisoService.CambiarEstado(100, true).GetAwaiter().GetResult());

            Assert.AreEqual("Aviso no existe.", ex.Message);
        }

        [Test]
        public void CreateAviso_debe_crear_aviso()
        {
            var avisoNew = new Aviso
            {                
                Titulo = "Aviso - new",
                AvisoCategoriaId = 2,
                AvisoTipoId = 2,
                Activo = false,
                FechaCreacion = DateTime.MinValue.AddDays(1),
                FechaDesde = DateTime.MinValue.AddDays(2),
                FechaHasta = DateTime.MinValue.AddDays(3),
                Texto = "texto aviso sesion - new",
                AvisosUsuarios = new List<AvisoUsuario>()
               {
                 new AvisoUsuario {Id = 5, UserName = "User05", FechaLeido = null}                 
               }
            };

            var result = _avisoService.CreateAsync(avisoNew).GetAwaiter().GetResult();

            Assert.Greater(result, 2);


            avisoNew.Id = result;
            avisoNew.AvisosUsuarios[0].Id = result;
            
            var aviso = _avisoService.GetAsync(result).GetAwaiter().GetResult();
            Assert.IsTrue(avisoNew.PropertiesAreEquals(aviso));


        }
    }
}
