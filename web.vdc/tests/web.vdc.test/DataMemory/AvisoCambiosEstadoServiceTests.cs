﻿using NUnit.Framework;
using System;
using Web.Vdc.Doctor.DataMemory;
using Web.Vdc.Doctor.Services;

namespace web.vdc.test.DataMemory
{
    public class AvisoCambiosEstadoServiceTests
    {
        private IAvisoCambioEstadoService _avisoCambioEstadoService;        

        [SetUp]
        public void Setup()
        {
            _avisoCambioEstadoService = new AvisoCambioEstadoService();            
        }

        [Test]
        public void CambiarEstado_debe_transcurrinr_menos_de_5segundos()
        {
            var start = DateTime.Now;
            var avisoId = 1;
            var estado = true;

            _avisoCambioEstadoService.CambiarEstado(avisoId, estado).GetAwaiter().GetResult();
            var end = DateTime.Now;

            Assert.Less(end.Subtract(start).TotalMilliseconds, 5000);
        }

       
    }
}
