﻿using NUnit.Framework;
using Web.Vdc.Doctor.DataMemory;
using Web.Vdc.Doctor.Services;

namespace web.vdc.test.DataMemory
{
    public class UsuarioServiceTests
    {

        private IUsuarioService _usuarioService;


        [SetUp]
        public void Setup()
        {

            _usuarioService = new UsuarioService();
        }
        [Test]
        public void GetTiposAvisos_debe_obtener_tipos()
        {
            var result = _usuarioService.GetUsuariosAsync().GetAwaiter().GetResult();

            Assert.AreEqual($"U-01 User01 1 1 R-01 medico",         $"{result[0].Id} {result[0].UserName} {result[0].ExternalUserId} {result[0].MedicoId} {result[0].RolId} {result[0].RolName}");
            Assert.AreEqual($"U-02 User02 2 2 R-01 medico",         $"{result[1].Id} {result[1].UserName} {result[1].ExternalUserId} {result[1].MedicoId} {result[1].RolId} {result[1].RolName}");
            Assert.AreEqual($"U-03 User03 3 3 R-01 medico",         $"{result[2].Id} {result[2].UserName} {result[2].ExternalUserId} {result[2].MedicoId} {result[2].RolId} {result[2].RolName}");
            Assert.AreEqual($"U-04 User04 4 0 R-02 visionmedico",   $"{result[3].Id} {result[3].UserName} {result[3].ExternalUserId} {result[3].MedicoId} {result[3].RolId} {result[3].RolName}");
            Assert.AreEqual($"U-05 User05 5 0 R-02 visionmedico",   $"{result[4].Id} {result[4].UserName} {result[4].ExternalUserId} {result[4].MedicoId} {result[4].RolId} {result[4].RolName}");
            Assert.AreEqual($"U-06 User06 6 0 R-03 admin",          $"{result[5].Id} {result[5].UserName} {result[5].ExternalUserId} {result[5].MedicoId} {result[5].RolId} {result[5].RolName}");
        }


    }
}
