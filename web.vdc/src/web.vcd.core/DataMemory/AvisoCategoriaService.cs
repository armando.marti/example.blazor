﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models.Avisos;
using Web.Vdc.Doctor.Services;

namespace Web.Vdc.Doctor.DataMemory
{
    public class AvisoCategoriaService : IAvisoCategoriaService
    {

        private readonly List<AvisoCategoria> AvisosCategorias = new List<AvisoCategoria>()
        {
            new AvisoCategoria{Id = 1, Descripcion = "Informativo"},
            new AvisoCategoria{Id = 2, Descripcion = "Alerta"},
            new AvisoCategoria{Id = 3, Descripcion = "Importante"}
        };
        public async Task<List<AvisoCategoria>> GetCategoriasAvisosAsync()
        {
            return await Task.FromResult(AvisosCategorias);
        }
    }

}
