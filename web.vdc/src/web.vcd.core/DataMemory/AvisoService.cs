﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models.Avisos;
using Web.Vdc.Doctor.Services;

namespace Web.Vdc.Doctor.DataMemory
{
    public class AvisoService : IAvisoService
    {
        private readonly IAvisoTipoService _tipoService;
        private readonly IAvisoCategoriaService _categoriaService;
        private readonly IUsuarioService _usuarioService;
        private readonly IAvisoCambioEstadoService _cambioEstadoService;


        private readonly List<Aviso> Avisos = new List<Aviso>()
        {
           new Aviso 
           { 
               Id = 1, Titulo = "Aviso inmediato 01", AvisoCategoriaId = 1, AvisoTipoId = 1, 
               Activo = true, FechaCreacion = new DateTime(2020,1,1), FechaDesde = new DateTime(2020,1,1).AddSeconds(5), FechaHasta = new DateTime(2020,1,1).AddDays(10), 
               Texto = "Aviso inmediato 01 informativo", 
               AvisosUsuarios = new List<AvisoUsuario>()
               {
                 new AvisoUsuario {Id = 1, AvisoId = 1, UserName = "User01", FechaLeido = null},
                 new AvisoUsuario {Id = 2, AvisoId = 1, UserName = "User02", FechaLeido = null}
               }
           },
           new Aviso
           {
               Id = 2, Titulo = "Aviso sesion 02", AvisoCategoriaId = 3, AvisoTipoId = 3, 
               Activo = true, FechaCreacion = new DateTime(2020,2,1).AddDays(3), FechaDesde = new DateTime(2020,2,1).AddSeconds(10), FechaHasta = new DateTime(2020,2,1).AddDays(10),
               Texto = "Aviso inicio sesión 02 importante",
               AvisosUsuarios = new List<AvisoUsuario>()
               {
                 new AvisoUsuario {Id = 3, AvisoId = 2, UserName = "User05", FechaLeido = null},
                 new AvisoUsuario {Id = 4, AvisoId = 2, UserName = "User06", FechaLeido = null}
               }
           }
        };

        public AvisoService(IAvisoTipoService tipoService, IAvisoCategoriaService categoriaService, IUsuarioService usuarioService, IAvisoCambioEstadoService cambioEstadoService)
        {
            _tipoService = tipoService;
            _categoriaService = categoriaService;
            _usuarioService = usuarioService;
            _cambioEstadoService = cambioEstadoService;
        }

        public Task<List<AvisoCategoria>> SourceCategorias => _categoriaService.GetCategoriasAvisosAsync();

        public Task<List<AvisoTipo>> SourceTipos => _tipoService.GetTiposAvisosAsync();

        public Task<List<Usuario>> SourceUsuarios => _usuarioService.GetUsuariosAsync();

        public Task<List<AvisoBase>> Source => Task.FromResult(Avisos.Select(s => new AvisoBase
        {
            Id = s.Id,
            Titulo = s.Titulo,
            AvisoTipoId = s.AvisoTipoId,
            Tipo = SourceTipos.GetAwaiter().GetResult().FirstOrDefault()?.Descripcion,
            AvisoCategoriaId = s.AvisoCategoriaId,
            Categoria = SourceCategorias.GetAwaiter().GetResult().FirstOrDefault()?.Descripcion,
            Activo = s.Activo,
            FechaCreacion = s.FechaCreacion,
            FechaHasta = s.FechaHasta,
            FechaDesde = s.FechaDesde
        }
        ).ToList());
        

        public Task CambiarEstado(int avisoId, bool estado)
        {
            var data = this.Avisos.FirstOrDefault(p => p.Id == avisoId);
            if (data == null) throw new Exceptions.ApiException("Aviso no existe.");

            return Task.FromResult(data.Activo = estado);
        }

        public Task<int> CreateAsync(Aviso item)
        {
            
            item.Id = this.Avisos.Count() + 1;
            
            this.Avisos.Add(item);

            return Task.FromResult(item.Id);
            
        }

        public Task<Aviso> GetAsync(int id)
        {
            var data = this.Avisos.FirstOrDefault(p => p.Id == id);
            if (data == null) throw new Exceptions.ApiException("Aviso no existe.");

            return Task.FromResult(data);
        }

        public Task UpdateAsync(Aviso item)
        {
            var data = this.Avisos.FirstOrDefault(p => p.Id == item.Id);
            if (data == null) throw new Exceptions.ApiException("Aviso no existe.");
            
            this.Avisos.Remove(data);
            this.Avisos.Add(item);
            
            return Task.FromResult(data);
            
        }
    }
}
