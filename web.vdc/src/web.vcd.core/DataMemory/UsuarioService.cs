﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models.Avisos;
using Web.Vdc.Doctor.Services;

namespace Web.Vdc.Doctor.DataMemory
{
    public class UsuarioService : IUsuarioService
    {
        private readonly List<Usuario> Usuarios = new List<Usuario>()
        {
            new Usuario{Id = "U-01", UserName = "User01", ExternalUserId = 1, MedicoId = 1, RolId = "R-01", RolName = "medico" },
            new Usuario{Id = "U-02", UserName = "User02", ExternalUserId = 2, MedicoId = 2, RolId = "R-01", RolName = "medico" },
            new Usuario{Id = "U-03", UserName = "User03", ExternalUserId = 3, MedicoId = 3, RolId = "R-01", RolName = "medico" },
            new Usuario{Id = "U-04", UserName = "User04", ExternalUserId = 4, MedicoId = 0, RolId = "R-02", RolName = "visionmedico" },
            new Usuario{Id = "U-05", UserName = "User05", ExternalUserId = 5, MedicoId = 0, RolId = "R-02", RolName = "visionmedico" },
            new Usuario{Id = "U-06", UserName = "User06", ExternalUserId = 6, MedicoId = 0, RolId = "R-03", RolName = "admin" }
        };

        public async Task<List<Usuario>> GetUsuariosAsync()
        {
            return await Task.FromResult(Usuarios);
        }
    }

}
