﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models.GetAvisos;
using Web.Vdc.Doctor.Services;

namespace Web.Vdc.Doctor.DataMemory
{
    public class AvisoMarcarLeidoService : IAvisosMarcarLeidoService
    {
        public RequestAvisoMarcarLeido Request { get; set; }

        public Task<ResponseMarcarLeido> MarcarLeido()
        {
            return Task.FromResult(new ResponseMarcarLeido() { AvisoId = 1, Success = true });
        }
    }
}
