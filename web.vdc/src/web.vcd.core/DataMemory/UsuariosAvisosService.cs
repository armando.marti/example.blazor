﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models.GetAvisos;
using Web.Vdc.Doctor.Services;

namespace Web.Vdc.Doctor.DataMemory
{
    public class UsuariosAvisosService : IUsuariosAvisosService
    {
        public RequestUsuariosAvisos Request { get; set; }

        public Task<List<ToastAviso>> GetAviso()
        {
            return Task.FromResult(new List<ToastAviso>() { new ToastAviso() { Texto = "Texto Aviso", Titulo = "Titulo aviso", FechaDesde = DateTime.Now.AddMinutes(-5), FechaHasta = DateTime.Now.AddMinutes(5), UserName = "master", TipoId = 1, CategoriaId = 1, Activo = true } });
        }
    }
}
