﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Exceptions;

namespace Web.Vdc.Doctor.Data
{
    public abstract class BaseApiService<IRequest, TModel> where TModel : class
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly string _apiClient;
        private readonly string _apiResource;
        private readonly string _actionName;

        public BaseApiService(IHttpClientFactory clientFactory, string apiClient, string apiResource, string actionName)
        {
            _clientFactory = clientFactory;
            _apiClient = apiClient;
            _apiResource = apiResource;
            _actionName = actionName;
        }

        public IRequest Request { get; set; }

        protected async Task<bool> ExecuteDeleteAsync()
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, $"{_apiResource}/{_actionName}{GetQueryString()}");
            var client = _clientFactory.CreateClient(_apiClient);

            var response = client.SendAsync(request).GetAwaiter().GetResult();

            bool result;
            if (response.IsSuccessStatusCode)
            {
                result = true;
            }
            else
            {
                throw new ApiException($"{GetConentResponse(response).GetAwaiter().GetResult().Detail}");
            }

            return result;
        }

        protected async Task<TModel> ExecutePatchAsync(bool withQueryString = false)
        {
            TModel result;

            HttpResponseMessage response = null;
            var client = _clientFactory.CreateClient(_apiClient);

            if (withQueryString)
            {
                var request = new HttpRequestMessage(HttpMethod.Patch, $"{_apiResource}/{_actionName}{GetQueryString()}");
                response = client.SendAsync(request).GetAwaiter().GetResult();
            }
            else
            {
                var stringPayload = await Task.Run(() => JsonSerializer.Serialize(Request));
                var message = new StringContent(stringPayload, System.Text.Encoding.UTF8, "application/json");
                response = client.PatchAsync($"{_apiResource}/{_actionName}", message).GetAwaiter().GetResult();
            }

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                result = await GetResult(responseStream);
            }
            else
            {
                throw new ApiException($"{GetConentResponse(response).GetAwaiter().GetResult().Detail}");
            }

            return result;
        }

        protected async Task<TModel> ExecutePutAsync()
        {
            TModel result;

            var client = _clientFactory.CreateClient(_apiClient);
            var stringPayload = await Task.Run(() => JsonSerializer.Serialize(Request));

            var message = new StringContent(stringPayload, System.Text.Encoding.UTF8, "application/json");
            var response = client.PutAsync($"{_apiResource}/{_actionName}", message).GetAwaiter().GetResult();

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                result = await GetResult(responseStream);
            }
            else
            {
                if (response.StatusCode == System.Net.HttpStatusCode.UnprocessableEntity)
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();
                    result = await GetResult(responseStream);
                }
                else
                {
                    throw new ApiException($"{GetConentResponse(response).GetAwaiter().GetResult().Detail}");
                }
            }

            return result;
        }

        protected async Task<TModel> ExecuteGetAsync()
        {
            TModel result;
            var request = new HttpRequestMessage(HttpMethod.Get, $"{_apiResource}/{_actionName}{GetQueryString()}");
            var client = _clientFactory.CreateClient(_apiClient);

            var response = client.SendAsync(request).GetAwaiter().GetResult();

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                result = await GetResult(responseStream);
            }
            else
            {
                throw new ApiException($"{GetConentResponse(response).GetAwaiter().GetResult().Detail}");
            }

            return result;
        }
        protected async Task<IEnumerable<TModel>> ExecuteGetIEnumerableAsync()
        {
            IEnumerable<TModel> result;
            var request = new HttpRequestMessage(HttpMethod.Get, $"{_apiResource}/{_actionName}{GetQueryString()}");
            var client = _clientFactory.CreateClient(_apiClient);

            var response = client.SendAsync(request).GetAwaiter().GetResult();

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                result = await GetIEnumerableResult(responseStream);
            }
            else
            {
                throw new ApiException($"{GetConentResponse(response).GetAwaiter().GetResult().Detail}");
            }

            return result;
        }

        protected async Task<IEnumerable<KeyValuePair<int, string>>> ExecuteGetListAsync()
        {
            IEnumerable<KeyValuePair<int, string>> result;
            var request = new HttpRequestMessage(HttpMethod.Get, $"{_apiResource}/{_actionName}{GetQueryString()}");
            var client = _clientFactory.CreateClient(_apiClient);

            var response = client.SendAsync(request).GetAwaiter().GetResult();

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                result = await GetIEnumerableListResult(responseStream);
            }
            else
            {
                throw new ApiException($"{GetConentResponse(response).GetAwaiter().GetResult().Detail}");
            }

            return result;
        }



        protected async Task<TModel> ExecutePostAsync(HttpContent content = null, bool HasContent = true)
        {
            TModel result;

            var client = _clientFactory.CreateClient(_apiClient);

            //con request en body
            var stringPayload = await Task.Run(() => JsonSerializer.Serialize(Request));
            var message = new StringContent(stringPayload, System.Text.Encoding.UTF8, "application/json");

            //con content
            var request = new HttpRequestMessage(HttpMethod.Post, $"{_apiResource}/{_actionName}");
            request.Content = content;

            var response = content == null ? client.PostAsync($"{_apiResource}/{_actionName}", message).GetAwaiter().GetResult() : client.SendAsync(request).GetAwaiter().GetResult();



            if (response.IsSuccessStatusCode)
            {
                if (HasContent)
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();
                    result = await GetResult(responseStream);
                }
                else
                {
                    result = null;
                }
            }
            else
            {
                throw new ApiException($"{GetConentResponse(response).GetAwaiter().GetResult().Detail}");
            }

            return result;
        }
        //protected async Task<string> ExecuteGetUrlAsync()
        //{
        //    var client = _clientFactory.CreateClient(_apiClient);
        //    return $"{client.BaseAddress}{_apiResource}/{_actionName}{GetQueryString()}";
        //}

        protected async Task<string> ExecuteGetUrlAsync(bool returnError = false)
        {
            string result;
            var request = new HttpRequestMessage(HttpMethod.Get, $"{_apiResource}/{_actionName}{GetQueryString()}");
            var client = _clientFactory.CreateClient(_apiClient);

            if (returnError == false) return $"{client.BaseAddress}{_apiResource}/{_actionName}{GetQueryString()}";

            var response = client.SendAsync(request).GetAwaiter().GetResult();

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                result = $"{client.BaseAddress}{_apiResource}/{_actionName}{GetQueryString()}";
            }
            else
            {
                throw new ApiException($"{GetConentResponse(response).GetAwaiter().GetResult().Detail}");
            }

            return result;
        }


        protected virtual async Task<TModel> GetResult(System.IO.Stream responseStream)
        {
            TModel result;

            var data = await JsonSerializer.DeserializeAsync<TModel>(responseStream);
            result = data;

            return result;
        }
        protected virtual async Task<IEnumerable<TModel>> GetIEnumerableResult(System.IO.Stream responseStream)
        {
            IEnumerable<TModel> result;

            var data = await JsonSerializer.DeserializeAsync<IEnumerable<TModel>>(responseStream);
            result = data;

            return result;
        }
        protected virtual async Task<IEnumerable<KeyValuePair<int, string>>> GetIEnumerableListResult(System.IO.Stream responseStream)
        {
            IEnumerable<KeyValuePair<int, string>> result;

            var data = await JsonSerializer.DeserializeAsync<IEnumerable<KeyValuePair<int, string>>>(responseStream);
            result = data;

            return result;
        }

        protected virtual async Task<ApiContentResponse> GetConentResponse(HttpResponseMessage response)
        {
            var result = new ApiContentResponse();

            try
            {
                var responseStream = await response.Content.ReadAsStreamAsync();
                var txtReader = new System.IO.StreamReader(responseStream);
                var reader = new Newtonsoft.Json.JsonTextReader(txtReader);
                var serializer = new Newtonsoft.Json.JsonSerializer();

                var data = serializer.Deserialize<ApiContentResponse>(reader);
                if (data != null)
                {
                    result.Status = data.Status;
                    result.Title = data.Title;
                    result.Detail = !string.IsNullOrWhiteSpace(data.Detail) ? data.Detail : data.error;
                    result.error = data.error;
                }

            }
            catch { }

            return result;

        }

        protected virtual string GetQueryString()
        {
            return "";
        }
    }
}
