﻿using System.Net.Http;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models.GetAvisos;
using Web.Vdc.Doctor.Services;

namespace Web.Vdc.Doctor.Data
{
    public class MarcarLeidoApiService : BaseApiService<RequestAvisoMarcarLeido, ResponseMarcarLeido>, IAvisosMarcarLeidoService
    {
        public MarcarLeidoApiService(IHttpClientFactory clientFactory)
            : base(clientFactory, "vdc.api", "Avisos", "MarcarLeido")
        {
        }

        public Task<ResponseMarcarLeido> MarcarLeido()
        {
            return ExecutePatchAsync(true);
        }

        protected override string GetQueryString()
        {
            return $"?avisoId={Request.AvisoId}&user={Request.User}";
        }

    }
}
