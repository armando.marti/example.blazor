﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models;
using Web.Vdc.Doctor.Models.Avisos;
using Web.Vdc.Doctor.Services;

namespace Web.Vdc.Doctor.Data
{
    public class UsuarioApiService : IUsuarioService
    {
        private readonly IHttpClientFactory _clientFactory;

        public UsuarioApiService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<List<Usuario>> GetUsuariosAsync()
        {
            IEnumerable<Usuario> result;
            var request = new HttpRequestMessage(HttpMethod.Get, "usuarios");
            var client = _clientFactory.CreateClient("vdc.api");

            var response = client.SendAsync(request).GetAwaiter().GetResult();

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                result = await JsonSerializer.DeserializeAsync
                        <IEnumerable<Usuario>>(responseStream);
            }
            else
            {
                throw new ApiException($"Api Error: {response.StatusCode} - {response.Content.ToString()}");
            }

            return result.ToList();
        }
    }
}
