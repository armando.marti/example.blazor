﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models.Avisos;
using Web.Vdc.Doctor.Services;

namespace Web.Vdc.Doctor.Data
{
    public partial class AvisoApiService : MaestroApiService<AvisoBase, Aviso>, IAvisoService
    {

        private readonly IAvisoTipoService _tipoService;
        private readonly IAvisoCategoriaService _categoriaService;
        private readonly IUsuarioService _usuarioService;
        private readonly IAvisoCambioEstadoService _cambioEstadoService;

        public AvisoApiService(IHttpClientFactory clientFactory,IAvisoTipoService tipoService, IAvisoCategoriaService categoriaService, IAvisoCambioEstadoService cambioEstadoService, IUsuarioService usuarioService) 
            : base(clientFactory, "vdc.api", "avisos", "Aviso")
        {
            _tipoService = tipoService;
            _categoriaService = categoriaService;
            _cambioEstadoService = cambioEstadoService;
            _usuarioService = usuarioService;
            Params.ApiQueryString = true;
        }

        public Task<List<AvisoCategoria>> SourceCategorias => _categoriaService.GetCategoriasAvisosAsync();

        public Task<List<AvisoTipo>> SourceTipos => _tipoService.GetTiposAvisosAsync();

        public Task<List<Usuario>> SourceUsuarios => _usuarioService.GetUsuariosAsync();

        public Task CambiarEstado(int avisoId, bool estado)
        {
            return Task.FromResult(_cambioEstadoService.CambiarEstado(avisoId, estado));
        }
    }
}
