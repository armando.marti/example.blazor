﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Web.Vdc.Doctor.ViewModels
{

    public enum ModelState 
    {
        Browsing = 0,
        Editing = 1,
        Creating = 2
    }
    
    public abstract class BaseViewModel : INotifyPropertyChanged
    {

        private bool isBusy = false;
        public bool IsBusy { get => isBusy; set => SetValue(ref isBusy, value); }
        
        private ModelState state = ModelState.Browsing;
        public ModelState State { get => state; set => SetValue(ref state, value); }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        protected void SetValue<T>(ref T backingFiled, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingFiled, value)) return;
            backingFiled = value;
            OnPropertyChanged(propertyName);
        }
    }
}
