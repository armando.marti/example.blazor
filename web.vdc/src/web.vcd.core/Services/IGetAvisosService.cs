﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models.GetAvisos;

namespace Web.Vdc.Doctor.Services
{
    public interface IGetAvisosService
    {
        public RequestUsuariosAvisos Request { get; set; }
        public List<ToastAviso> AvisosPendientes { get; set; }
        public void GetAvisosPendientes();
        public event Action OnChange;
    }

    public interface IUsuariosAvisosService : IRequestUsuariosAvisos
    {
        Task<List<ToastAviso>> GetAviso();
    }

    public interface IAvisosMarcarLeidoService : IRequestAvisosMarcarLeido
    {
        Task<ResponseMarcarLeido> MarcarLeido();
    }
}
