﻿namespace Web.Vdc.Doctor.Services
{
    public class ParamRequest
    {
        public ParamRequest()
        {
            OrderColumnName = "id";
            OrderDirection = "ascending";
            
            SearchContent = string.Empty;
            WhereContent = string.Empty;
            WhereChildContent = string.Empty;
            WhereParentContent = string.Empty;
            WhereNodo = string.Empty;
            
            TakeRows = int.MaxValue;
            SkipRows = 0;
            Count = 0;

            ApiQueryString = false;
            
        }

        public ParamRequest(string orderColumnName, string orderDirection, string searchContent, int topRows, int skipRows, int count, bool apiQueryString)
        {
            OrderColumnName = orderColumnName;
            OrderDirection = orderDirection;
            
            SearchContent = searchContent;
            
            TakeRows = topRows;
            SkipRows = skipRows;
            Count = count;

            ApiQueryString = apiQueryString;
            
            
        }

        public string OrderColumnName { get; set; }
        public string OrderDirection { get; set; }
        public string SearchContent { get; set; }
        public string WhereContent { get; set; }
        public string WhereParentContent { get; set; }
        public string WhereChildContent { get; set; }
        public string WhereNodo { get; set; }
        public int TakeRows { get; set; }
        public int SkipRows { get; set; }
        public int Count { get; set; }
        public bool ApiQueryString { get; set; }
        
    }
}
