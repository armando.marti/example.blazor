﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models.Avisos;

namespace Web.Vdc.Doctor.Services
{
    public interface IAvisoService : IMaestroService<AvisoBase, Aviso>
    {
        Task<List<AvisoCategoria>> SourceCategorias {get;}
        Task<List<AvisoTipo>> SourceTipos { get; }
        Task<List<Usuario>> SourceUsuarios { get; }

        Task CambiarEstado(int avisoId, bool estado);

    }
}
