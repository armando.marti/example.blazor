﻿using System.Text.Json.Serialization;

namespace Web.Vdc.Doctor.Models
{
    public class DisponibleAltasEstructuraCliente
    {
        [JsonPropertyName("Key")]
        public int Id { get; set; }

        [JsonPropertyName("Value")]
        public string Nombre { get; set; }
    }
}
