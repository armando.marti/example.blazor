﻿namespace Web.Vdc.Doctor.Models.Avisos
{
    public class AvisoTipo
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
