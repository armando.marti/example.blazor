﻿namespace Web.Vdc.Doctor.Models.Avisos
{
    public class Usuario
    {
        public string Id { get; set; }
        public string RolId { get; set; }
        public string UserName { get; set; }
        public string RolName { get; set; }
        public int? ExternalUserId { get; set; }
        public int? MedicoId { get; set; }
    }
}
