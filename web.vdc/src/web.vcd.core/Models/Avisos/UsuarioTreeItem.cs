﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Vdc.Doctor.Models.Avisos
{
    
    public class UsuarioTreeItem
    {
        public UsuarioTreeItem()
        {
        }

        public UsuarioTreeItem(List<Usuario> usuarios)
        {
            
            var roles = usuarios.Select(p=> new { p.RolId, p.RolName }).Distinct().OrderBy(p => p.RolName).ToList();

            int id = 100;
            var rolId = 1;

            foreach (var rol in roles)
            {                
                var childs = usuarios.Where(p => p.RolId == rol.RolId).OrderBy(p=>p.UserName).Select(p => new TreeItem{Id = id++, ParentId = rolId, Name = p.UserName}).Distinct().ToList();
                
                this.TreeItems.Add(new TreeItem { Id = rolId++, Name = rol.RolName, Child = childs, Expanded = false, HasChild = true });
                
            }
        }
        public void SetState(List<string> users)
        {
            foreach (var rol in TreeItems)
            {                
                foreach (var user in rol.Child)
                {
                    user.IsChecked = false;
                    if (users.Contains(user.Name)) user.IsChecked = true;
                }
                rol.IsChecked = (rol.Child.All(p => p.IsChecked));                
            }
        }
        public List<TreeItem> TreeItems { get; set; } = new List<TreeItem>();
    }

    public class TreeItem
    {
        public TreeItem()
        {
        }

        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public bool HasChild { get; set; }
        public bool Expanded { get; set; }
        public bool Selected { get; set; }
        public bool IsChecked { get; set; }
        public List<TreeItem> Child { get; set; } = new List<TreeItem>();

    }
}
