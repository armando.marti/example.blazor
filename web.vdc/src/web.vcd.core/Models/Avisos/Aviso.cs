﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;

namespace Web.Vdc.Doctor.Models.Avisos
{   
    public class AvisoBase : MaestroBase
    {
        
        public string Titulo { get => Nombre; set => Nombre = value; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }
        public int? AvisoTipoId { get; set; }
        public string Tipo { get; set; }
        public int? AvisoCategoriaId { get; set; }
        public string Categoria { get; set; }
        public bool Activo { get; set; }
    }
    
    public class Aviso : ModelBase
    {
        public Aviso()
        {
            AvisosUsuarios = new List<AvisoUsuario>();
        }

        [Required(ErrorMessage = "'{0}' es requerido")]
        [Display(Name = "Título", ShortName = "Título")]
        [MaxLength(50, ErrorMessage = "'{0}' debe tener una longitud máxima de 50 caracteres.")]
        public string Titulo { get; set; }
        public DateTime? FechaCreacion { get; set; }
        [Required(ErrorMessage = "'{0}' es requerida")]
        [Display(Name = "Fecha Desde", ShortName = "F.Desde")]
        public DateTime? FechaDesde { get; set; }

        [Required(ErrorMessage = "'{0}' es requerida")]
        [Display(Name = "Fecha Hasta", ShortName = "F.Hasta")]
        public DateTime? FechaHasta { get; set; }
        
        [Required(ErrorMessage = "'{0}' es requerido")]
        [Display(Name = "Tipo del Aviso", ShortName = "T.Aviso")]
        [Range(1, int.MaxValue, ErrorMessage = "'{0}' es requerido")]
        public int? AvisoTipoId { get; set; }

        [Required(ErrorMessage = "'{0}' es requerida")]
        [Display(Name = "Categoría del Aviso", ShortName = "C.Aviso")]
        [Range(1, int.MaxValue, ErrorMessage = "'{0}' es requerida")]
        public int? AvisoCategoriaId { get; set; }        
        public bool Activo { get; set; }

        [Required(ErrorMessage = "'{0}' es requerido")]
        [Display(Name = "Texto", ShortName = "Texto")]
        [StringLength(200, ErrorMessage = "'{0}' debe tener una longitud máxima de 200 caracteres.")]        
        public string Texto { get; set; }
        
        [Display(Name = "Usuarios", ShortName = "Usuarios")]
        [HasElements(ErrorMessage = "'{0}' debe tener al menos un usuario.")]
        public List<AvisoUsuario> AvisosUsuarios { get; set; }

        public void SetUser(string userName, bool activo)
        {
            if (AvisosUsuarios == null) AvisosUsuarios = new List<AvisoUsuario>();

            AvisosUsuarios.RemoveAll(p => p.UserName == userName);
            if (activo) AvisosUsuarios.Add(new AvisoUsuario { AvisoId = this.Id, UserName = userName });
            
                
        }
    }
}
