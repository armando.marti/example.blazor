﻿using System;

namespace Web.Vdc.Doctor.Models
{
    public class WebException : Exception
    {
        public WebException(string message) : base(message)
        {
        }
    }
}
