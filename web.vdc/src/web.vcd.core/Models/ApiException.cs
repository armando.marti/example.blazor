﻿using System;

namespace Web.Vdc.Doctor.Models
{
    public class ApiException : Exception
    {
        public ApiException(string message) : base(message)
        {
        }
    }
}
