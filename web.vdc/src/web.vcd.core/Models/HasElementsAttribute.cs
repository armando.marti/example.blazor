﻿using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace Web.Vdc.Doctor.Models
{
    public class HasElementsAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {

            var list = value as IList;

            if (list != null)
                return list.Count > 0;

            return false;

        }
    }
}
