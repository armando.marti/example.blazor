﻿using System;

namespace Web.Vdc.Doctor.Exceptions
{
    public class ApiException : Exception
    {
        public ApiException(string message) : base(message)
        {
        }
    }
}
