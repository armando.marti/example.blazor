﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.DependencyInjection;
using Syncfusion.Blazor;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Data;

namespace Web.Vdc.Doctor.Dependencyinjection
{
    public static class LocalizationExtensions
    {

        public static IServiceCollection AddLocalizationServices(this IServiceCollection services)
        {
            #region Localization
            // Set the Resx file folder path to access
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddSyncfusionBlazor();
            // register a Syncfusion locale service to customize the Syncfusion Blazor component locale culture
            services.AddSingleton(typeof(ISyncfusionStringLocalizer), typeof(ResourcesLocalizer));
            services.Configure<RequestLocalizationOptions>(options =>
            {
                // Define the list of cultures your app will support
                var supportedCultures = new List<CultureInfo>()
            {
                new CultureInfo("en-US"),
                new CultureInfo("es")
            };

                // Set the default culture
                options.DefaultRequestCulture = new RequestCulture("es");

                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });
            #endregion

            // register a custom localizer for blazor components, after registering the blazor services
            services.AddSingleton(typeof(ISyncfusionStringLocalizer), typeof(ResourcesLocalizer));

            return services;
        }
    }
}
