﻿using Microsoft.Extensions.DependencyInjection;
using Web.Vdc.Doctor.Data;
using Web.Vdc.Doctor.DataMemory;
using Web.Vdc.Doctor.Services;
using Web.Vdc.Doctor.ViewModels;

namespace Web.Vdc.Doctor.Dependencyinjection
{
    public static class DomainExtensions
    {
        public static IServiceCollection AddDomainServices(this IServiceCollection services, bool inMemory = true, double refreshAvisos = 60000)
        {

            services.AddSingleton<TokenContainer>();

            services.AddTransient<ITimerService, TimerService>();


            if (inMemory)
            {
                SetMemoryServices(services);
            }
            else
            {
                SetApiServices(services);
            }


            //view-models
            services.AddScoped<IAvisoViewModel, AvisoViewModel>();

            //servicio para recibir Avisos programados
            services.AddScoped<IGetAvisosService>(x => new GetAvisosService(x.GetRequiredService<IUsuariosAvisosService>(), x.GetRequiredService<ITimerService>(), refreshAvisos));

            return services;

        }

        private static void SetApiServices(IServiceCollection services)
        {
            

            //avisos
            services.AddScoped<IAvisoTipoService, AvisoTipoApiService>();
            services.AddScoped<IAvisoCategoriaService, AvisoCategoriaApiService>();
            services.AddScoped<IAvisoCambioEstadoService, AvisoCambioEstadoApiService>();
            services.AddScoped<IAvisoService, AvisoApiService>();

            //usuarios
            services.AddScoped<IUsuarioService, UsuarioApiService>();            

            //GetAvisos (toast)
            services.AddScoped<IUsuariosAvisosService, UsuariosAvisosApiService>();
            services.AddScoped<IAvisosMarcarLeidoService, MarcarLeidoApiService>();
            

            


        }

        private static void SetMemoryServices(IServiceCollection services)
        {
            
            //avisos
            services.AddScoped<IAvisoTipoService, AvisoTipoService>();
            services.AddScoped<IAvisoCategoriaService, AvisoCategoriaService>();
            services.AddScoped<IAvisoCambioEstadoService, AvisoCambioEstadoService>();
            services.AddScoped<IAvisoService, AvisoService>();

            //usuarios
            services.AddScoped<IUsuarioService, UsuarioService>();
            

            //GetAvisos (toast)
            services.AddScoped<IUsuariosAvisosService, UsuariosAvisosService>();
            services.AddScoped<IAvisosMarcarLeidoService, AvisoMarcarLeidoService>();
            

        }
    }
}
